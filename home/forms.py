from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms

# import model dari models.py

class PostSignUp(UserCreationForm):
    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password1',
            'password2',
        ]

        widgets = {
            'username': forms.TextInput(
                attrs= {
                    'class':'form-control',
                    'placeholder':'',
                }
            ),
            'email': forms.EmailInput(
                attrs= {
                    'class':'form-control',
                    'placeholder':'',
                }
            ),
            'password1': forms.PasswordInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': ''
                }
            ),
            'password2': forms.PasswordInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': ''
                }
            ),

        }