from django.urls import path
from django.conf.urls import url 

from . import views

app_name = 'home'

urlpatterns = [
    path('', views.signup, name='signup'),
    path('login/', views.login, name='login'),
    path('index/', views.index, name='index'),
    path('logout/', views.logout_view, name='logout'),
]