from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.contrib.auth.models import User
# Create your tests here.

from .views import login, logout_view, signup

class TestLogin(TestCase):
    def test_login_url_page_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_login_func(self):
        found = resolve("/login/")
        self.assertEqual(found.func, login)

    def test_login_using_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'home/login.html')

    def test_login_success(self):
        response = Client()
        user = User.objects.create(username='bintang.samudro', email='samudrobintang10@gmail.com')
        user.set_password('barcelona10')
        user.save()
        logged_in = response.login(username='bintang.samudro', password='barcelona10')
        self.assertTrue(logged_in)

class TestSignUp(TestCase):
    def test_signup_url_page_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_signup_func(self):
        found = resolve("/")
        self.assertEqual(found.func, signup)

    def test_signup_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home/signup.html')

    def test_signup_success(self):
        response = Client()
        user = User.objects.create(username='bintang.samudro', email='samudrobintang10@gmail.com')
        user.set_password('barcelona10')
        user.save()
        self.assertEqual(User.objects.all().count(), 1)

class TestLogout(TestCase):
    def test_logout_url_page_is_exist(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)

    def test_logout_func(self):
        found = resolve("/logout/")
        self.assertEqual(found.func, logout_view)
