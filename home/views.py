from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.http import HttpResponse
from django.contrib import messages
from django.contrib.auth import authenticate, logout
from django.contrib.auth import login as dj_login
from .forms import PostSignUp
from django.contrib.auth.decorators import login_required
import requests


# Create your views here.
def index(request):
    return render(request, 'home/index.html')

def signup(request):
    form = PostSignUp()

    if request.method == 'POST':
        form = PostSignUp(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, 'Akun berhasil dibuat untuk ' + username)

            return redirect('/login/')

    context = {'form' : form}
    return render(request, 'home/signup.html', context)

def login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            dj_login(request, user)
            return redirect('/index/')
        else:
            messages.info(request, 'Username or Password is incorrect')

    # context = {}
    return render(request, 'home/login.html')


def logout_view(request):
    logout(request)
    return redirect('/login/')